---
title: Add Support of Unix Permission in the Shmdata Library
slug: proposal-vanshitaverma-add-support-of-unix-permission-in-the-shmdata-library.md
author: Vanshita Verma
date: 2022-04-19 11:24:11 UTC-05:00
tags: proposals, medium, 175 hours
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Vanshita Verma
* gitlab username: vanshitaverma
* timezone: GMT +5:30

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
-->

Add Support of Unix Permission in the Shmdata Library

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

The Shmdata library provides a layer to share streams of framed data between processes via shared memory. It supports any kind of data stream: it has been used with multichannel audio, video frames, 3D models, OSC messages, and various others types of data. Shmdata is server-less, but requires applications to link data streams using socket path (e.g. "/tmp/my-shmdata-stream"). Shmdata is very fast and allows processes to access data streams without the need for extra copies.

However, permissions are not supported, and any stream shared on the system with Shmdata is accessible system-wide. The project will enable the possibility for a Shmdata user to configure streams permission using shared memory right setting at the API level.

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

Currently Shmdata does not support unix permissions and is accessible system-wide for any stream shared with Shmdata. The project will enable us to set permissions and restrict access to certain data in the library.

User who access Shmdata will be able to manage permissions for the data they are sending across the shared memory.

Different permissions to access data and files in the system will benefit the organistion in maintaining control over the library and the users to data privacy.


## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

* Providing an option for users to restrict or openly share data streams via these file/ directory access modes: read, write and execute/traverse.

* Changing data access permissions.

* Documentation for the improvements made.

* Incorporation of ACL to assist with Unix file permissions. (optional/if time allows) 

ABOUT THE PROJECT

The addition of Unix permission would offer data privacy. The data accessed can be viewed everyone as of now.

When sharing data between processes via shared memory, not all components of the data need to be visible to everyone. Different groups might need to access different data, furthermore, some data might only be needed to be visible to the owner of the files. In such cases, the permissions that would have to be implemented are combinations of 'read', 'write' and 'execute' permissions that would be given to the owner, group or common user(i.e. everyone else).

Permissions would be 'none', 'execute only', 'write only', 'write and execute', 'read only', 'read and execute', 'read and write', 'read, write and execute' with corresponding values of 0-7 respectively.

The documentation for the code written for this addition of unix permissions support will be written throughout the weeks of coding period.

TIMELINE

May 20th 2022 - June 12th 2022 (Community Bonding Period)
Getting well versed with the Shmdata library and its code style & structure while finishing writing my university end-term examinations.

June 13th 2022 - June 26th 2022
Investigation (thinking) time.

June 27th - July 17th (Coding Period)
Implementing file and ownership permissions. Have figured out the segregation attributes of groups for owner, group (different levels) and user permissions.
Setting up default permissions for new files/directories

July 25th - July 29th (Phase 1 Evaluation)
Code the file access modes namely, read, write and execute permissions pertaining to the segregation of different attributes.

July 30th - August 7th 2022 
Have written the code for the directory access modes (read, write and traverse) for the directories at the API level. 

August 8th - August 21st 2022
Figure out a way to implement changing access modes in the file systems and directories. Configure stream permissions for user to use shared memory right settings.

August 22nd - September 4th 2022
Final documentation and refinement of code.

September 5th 2022 - September 12th 2022 (Final Week)
Final evaluation report and code submission.


## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

Shmdata is a library which helps share streams of data. This data is sent via shared memory by users. However, since Shmdata does not support Unix permissions so everyone can access the data shared. This makes it a problem for data privacy and thus comes the need to add support for Unix permissions. 

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

My name is Vanshita Verma and I am a sophomore Computer Science engineering student from India. I am a woman in STEM with a fascination towards arts & music, with classical training in music vocals. All of this made me gravitate towards your organisation.

I am a beginner in the Open Source world and this would be my first time contributing to an Open Source project. I have however been programming for about 4 years now (with an avid interest in competitive programming). I have done multiple courses pertaining to the same.

I have previously been a dedicated performer in fields that i commit to and I will be able to dedicate around 20-30 hours per week into making this project successful.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* C++
* SysV Shared Memory, Unix Socket programming
* interest/experience with audio/video/datastream programming
* interest/experience with Python C API
* interest/experience with GStreamer element writing

I have studied Computer Science in high school. C++ was the programming language in which I learnt (from basic to intermediate level) coding for two years. While in university, I have become experienced in C, Python and Java. 

I am familiar with Unix/Linux system programming which I have become experienced with while studying Operating Systems as a subject this semester.

I am interested in GStreamer which i will learn more about during the next few weeks before the Community Bonding period begins.

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


175 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

medium
