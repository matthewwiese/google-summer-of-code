<!--
.. title: Guidelines for GSoC process and workflow at SAT
.. slug: intro
.. author: Christian Frisson
.. date: 2022-03-16 16:18:11 UTC-04:00
.. tags: guidelines
.. type: text
-->

The purpose of these guidelines is to extend recommendations from official GSoC resources such as the [contributor](https://google.github.io/gsocguides/student/) and [mentor](https://google.github.io/gsocguides/mentor/) guides, and to complement these for the particular workflow for GSoC with the SAT.

We aim at keeping our workflow and process as open and transparent as possible.

This list will grow as we learn throughout the first year (2022) for SAT as a GSoC organization. 

* onboarding contributors on gitlab and Matrix
* [transitioning from ideas applications through gitlab issues to proposals applications through merge requests](/pages/guidelines/from-ideas-to-proposals/)
* defining criteria for ranking contributor proposals
* following a code of conduct
* [framing evaluations](/pages/guidelines/framing-evaluations/) between Contributors and Mentors

Please review and open [issues](https://gitlab.com/sat-mtl/google-summer-of-code/-/issues) or [merge requests](https://gitlab.com/sat-mtl/google-summer-of-code/-/merge_requests) to help us clarify your doubts and improve these guidelines.
