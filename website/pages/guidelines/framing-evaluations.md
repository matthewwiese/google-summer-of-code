<!--
.. title: Guidelines: framing evaluations
.. slug: framing-evaluations
.. author: Christian Frisson, Alexandra Marin
.. date: 2022-06-13 11:22:11 UTC-04:00
.. tags: guidelines
.. type: text
-->

These guidelines support the process of guiding Contributors and Mentors on how to frame their mutual evaluations throughout their GSoC project (mid-term and final). 

# Background

These guidelines extend recommendations from official GSoC resources, particularly the [Google's GSoC Help Evaluation page](https://developers.google.com/open-source/gsoc/help/evaluations).

# Evaluation Questions

These are mainly copied from this [Google's GSoC Help Evaluation page](https://developers.google.com/open-source/gsoc/help/evaluations), last updated 2022-04-14 UTC. 

> Except as otherwise noted, the content of this page is licensed under the Creative Commons Attribution 4.0 License, and code samples are licensed under the Apache 2.0 License. For details, see the Google Developers Site Policies. Java is a registered trademark of Oracle and/or its affiliates.

> There are two times during the coding phase of GSoC where mentors evaluate their contributors' progress and experience and contributors evaluate their mentors.

> Below are **examples of the questions that may be asked during each of these evaluations**.

Note: **we highlight some criteria and preferences specific to SAT in bold next to questions.**

## Mentor

> Only one evaluation can be submitted per GSoC contributor project. If there are multiple mentors they can all see the evaluation and edit it but the last updates before the deadline takes precedence.

### Midterm

* When did you first communicate with your GSoC contributor?
* How often do you and your GSoC contributor communicate? **(synchronously: ideally weekly, or bi-weekly/monthly; async: on demand)**
* How do you communicate with your GSoC contributor? **(synchronously: Google Meet or Matrix; async: gitlab issues for public information, or email for private communications)**
* How many hours do you spend on your GSoC contributor's project per week?
* How often do you require status updates from your GSoC contributor? **(ideally weekly, or bi-weekly/monthly)**
* Rate the quality of your GSoC contributor's interactions with the project mentor(s)
* Please rate the quality of your GSoC contributor's interactions with your organization and community.
* How would you rate your GSoC contributor's performance overall?
* Is your GSoC contributor on track to complete their project?
* What is the quality of code/work your GSoC contributor has produced so far?
* For this evaluation, should your GSoC contributor pass or fail?
* (If passed) Are you confident in your decision to pass your GSoC contributor?
* (If failed) Why did you fail your GSoC contributor?
* Feedback for your GSoC contributor (will be shared directly with GSoC contributor)
* Anything else you'd like to tell Google?

### Final

* Did your GSoC contributor complete this project?
* On average, how many hours do you spend on this GSoC contributor project per week? **(30 minutes to 1 hour for weekly meetings, plus a few hours on code as needed)**
* Did your GSoC contributor correctly upload their work? Please verify that the link above clearly points to the contributor’s work and meets the work submission guidelines. Someone looking at that link should be able to tell that this was work completed for GSoC? Invalid or missing submissions should result in failing this evaluation. **(this link will be hosted by our gitlab pages, using [work-product-template.md](https://gitlab.com/sat-mtl/google-summer-of-code/-/blob/main/work-product-template.md), please read these [instructions](https://gitlab.com/sat-mtl/google-summer-of-code#contributions) that Contributors need to follow)**
* How would you rate your GSoC contributor's performance overall?
* For the final evaluation, should your GSoC contributor pass or fail?
* (If passed) Are you confident in your decision to pass this GSoC contributor?
* (If failed) Why did you fail your student?
* How do you expect your GSoC contributor's project to impact your org in the long term? (examples: was a critical patch, a standalone module, a new feature, etc.)
* I believe GSoC is valuable for my organization (ranking options)
* I believe GSoC is valuable for open source (ranking options)
* Feedback for your GSoC contributor (shared directly with them)
* What advice would you give to future GSoC contributors?
* What advice would you give to future GSoC mentors?
* What kind of advice would you give contributors and mentors on project scope and focus?
* We're interested in your thoughts on 3 big changes we made to GSoC in 2022?
* Which of these change did you like?
* Any other feedback about these changes?
* Anything else you'd like to tell Google or suggestions on how we could improve the program?

## Contributor

### Midterm

* Were you a GSoC student in 2021?
* If yes, which org did you work with?
* Is your 2022 project a continuation of your previous year's project?
* Did you contribute to this organization before GSoC 2022 began?
* For how many months did you contribute to this org before March 7, 2022?
* When did you first communicate with your organization this year?
* How many weeks before accepted orgs were announced did you first communicate with the org?
* How often, on average, do you interact with your mentor(s)? **(30 minutes to 1 hour for weekly meetings, plus a few hours on code as needed)**
* How do you communicate with your mentor(s)? **(synchronously: Google Meet or Matrix; async: gitlab issues for public information, or email for private communications)**
* What is your preferred method of communicating with your mentor? (shared w/Mentor and Org Admin)
* Rate the quality of interactions with your mentor(s)
* Was the community bonding period before coding began helpful for you?
* What would have made the community bonding period more helpful/a better experience for you?
* What is the most challenging part of participating in GSoC so far?
* Feedback for your organization and mentors
* How responsive are your mentor(s)? (shared only w/Org Admin)
* Anything else you'd like to tell Google?

### Final

* Provide a link to the work you did during GSoC 2022. **(this link will be hosted by our gitlab pages, using [work-product-template.md](https://gitlab.com/sat-mtl/google-summer-of-code/-/blob/main/work-product-template.md), Contributors please read these [instructions](https://gitlab.com/sat-mtl/google-summer-of-code#contributions))**
* Considering your original project plan, how close are you to having met all the goals you originally outlined?
* How many total hours do you think you spent working on your project since the coding period began? (Do not include planning during the community bonding period)
* Did you have a job or internship during GSoC 2022? (Part time or full time.)
* Were you a student in an academic program when the coding period began?
* How many hours of classes were you taking during the GSoC coding period when you were an active student?
* What is your favorite part of participating in GSoC?
* What do you consider the most challenging part of participating in GSoC?
* Would you apply for GSoC again (if you were eligible)?
* How would you rate your experience with GSoC overall?
* Would you recommend your mentor(s) to other contributors?
* Do you plan to continue to contribute to this organization?
* Did you have any problems with your mentor or the organization?
* Did any of these GSoC 2022 changes affect your decision or ability to apply for GSoC?
* Will you continue to participate in open source?
* Has your work in GSoC helped you get a job offer or internship?
* Has GSoC helped improve your programming skills?
* Tell us how GSoC helped improve your programming skills.
* Would you consider being a mentor or org admin for GSoC?
* Did the communications from Google Program Administrators during the program keep you informed?
* What advice would you give to contributors participating in GSoC in the future?
* Anything else you'd like to tell Google or suggestions on how we could improve the program?
* Feedback for this organization and mentors
* What could your mentor(s) or this organization do differently to better help contributors in the future? (shared with Mentors and Org Admin)
* Any general feedback for **SAT**? (What they could do better or what they excelled at) (shared with organization administrators only)

# Evaluation Criteria at SAT

Here are examples of criteria elicited at SAT and that may guide Mentors to frame their evaluations.

## Communication

* Contributor answers promptly (within 48 hours).
* Contributor uses the right communication tools for the right purpose.
* Contributor remains available for weekly or biweekly meetings.
* Contributor keeps a communication channel open and is reachable for weekly communications (not necessarily in real time).

## Development

* Contributor has pushed a few merge requests and most of them got merged.
* Contributor has overall respected the contribution guides (a few mistakes permitted.
* Contributor's work preserves existing code and CI tests. 
* Contributor's contributions come with at least one test in the CI.
* Contributor avoids gross misconduct, against Code of Conduct (examples: deleting code/issues, offensive language repeatedly... Mentors should be lenient if such mistakes happen once).
* Contributor's work is modular and chunked into small blocks of progress.
* Contributor keeps in mind that their code will be deployed or ported to other platforms (example: Raspberry Pi), so cross-platform solutions should be preferred.

## Documentation

* Contributor documents every new feature.

## Contribution to the community 

* Contributor desires to learn and contribute to the community. 
* Contributor shares their work through blog posts.

## Other considerations

* Contributor is equipped with required skills as mentioned in their proposal (and claimed in their explanation of their background). 
